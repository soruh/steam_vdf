use std::{
    collections::HashMap,
    io::{self, Read, Write},
    path::Path,
};

pub struct Writer<W: Write> {
    indent: bool,
    depth: u32,
    writer: W,
}

impl<W: Write> Writer<W> {
    pub fn new(indent: bool, writer: W) -> Self {
        Self {
            indent,
            writer,
            depth: 0,
        }
    }

    pub fn write(&mut self, kind: &str, data: &HashMap<String, Value>) -> io::Result<()> {
        self.write_string(kind)?;

        self.newline()?;

        self.write_dict(data)?;

        writeln!(self.writer)
    }

    fn write_dict_field(&mut self, key: &str, value: &Value) -> io::Result<()> {
        self.write_string(key)?;

        match value {
            Value::Dict(_) => self.newline()?,
            Value::String(_) => self.tab()?,
        }

        self.write_value(value)
    }

    fn write_string(&mut self, string: &str) -> io::Result<()> {
        write!(self.writer, "\"")?;

        for c in string.chars() {
            if let Some(c) = escape_char(c) {
                write!(self.writer, "\\{}", c)?;
            } else {
                write!(self.writer, "{}", c)?;
            }
        }

        write!(self.writer, "\"")
    }

    fn newline(&mut self) -> io::Result<()> {
        if self.indent {
            writeln!(self.writer)?;

            for _ in 0..self.depth {
                write!(self.writer, "\t")?;
            }

            Ok(())
        } else {
            write!(self.writer, " ")
        }
    }

    fn tab(&mut self) -> io::Result<()> {
        if self.indent {
            write!(self.writer, "\t\t")
        } else {
            write!(self.writer, " ")
        }
    }

    fn write_dict(&mut self, dict: &HashMap<String, Value>) -> io::Result<()> {
        write!(self.writer, "{{")?;

        self.depth += 1;
        for (key, value) in dict.iter() {
            self.newline()?;

            self.write_dict_field(key, value)?;
        }
        self.depth -= 1;

        self.newline()?;

        write!(self.writer, "}}")?;

        Ok(())
    }

    fn write_value(&mut self, value: &Value) -> io::Result<()> {
        match value {
            Value::String(string) => self.write_string(string),
            Value::Dict(dict) => self.write_dict(dict),
        }
    }
}

pub fn parse(input: &str) -> Option<(String, Value)> {
    let (res, remainder) = parse_dict_field(input.trim_start())?;

    remainder.trim_start().is_empty().then(|| res)
}

pub fn from_file(file: impl AsRef<Path>, kind: &str) -> std::io::Result<HashMap<String, Value>> {
    let file = std::fs::read_to_string(file)?;

    from_string(&file, kind)
}

pub fn from_reader<R: Read>(mut reader: R, kind: &str) -> std::io::Result<HashMap<String, Value>> {
    let mut string = String::new();
    reader.read_to_string(&mut string).unwrap();
    from_string(&string, kind)
}

pub fn from_string(string: &str, kind: &str) -> std::io::Result<HashMap<String, Value>> {
    if let Some((filetype, Value::Dict(content))) = parse(string) {
        if filetype == kind {
            return Ok(content);
        }
    }

    Err(std::io::Error::new(
        std::io::ErrorKind::Other,
        "Corrupt acf file",
    ))
}

#[derive(Debug, PartialEq, Eq)]
pub enum Value {
    String(String),
    Dict(HashMap<String, Value>),
}

#[allow(clippy::wrong_self_convention)]
impl Value {
    pub fn as_dict(self) -> Option<HashMap<String, Value>> {
        if let Value::Dict(x) = self {
            Some(x)
        } else {
            None
        }
    }

    pub fn as_string(self) -> Option<String> {
        if let Value::String(x) = self {
            Some(x)
        } else {
            None
        }
    }
}

fn unescape_char(c: char) -> char {
    match c {
        '\\' => '\\',
        'n' => '\n',
        't' => '\t',
        'r' => '\r',
        '"' => '"',
        c => c,
    }
}

fn escape_char(c: char) -> Option<char> {
    match c {
        '\\' => Some('\\'),
        '\n' => Some('n'),
        '\t' => Some('t'),
        '\r' => Some('r'),
        '"' => Some('"'),
        _ => None,
    }
}

fn parse_string(input: &str) -> Option<(String, &str)> {
    let input = input.strip_prefix('"')?;

    let mut res = String::new();
    let mut escaped = false;
    for (i, c) in input.char_indices() {
        if escaped {
            res.push(unescape_char(c));
            escaped = false;
        } else if c == '\\' {
            escaped = true;
        } else if c == '"' {
            // this is okay since the char at i is always " and thus 1 byte wide
            return Some((res, input[i + 1..].trim_start()));
        } else {
            res.push(c);
        }
    }

    None
}

fn parse_dict(input: &str) -> Option<(HashMap<String, Value>, &str)> {
    let mut input = input.strip_prefix('{')?.trim_start();

    let mut map = HashMap::new();
    while input.chars().next()? != '}' {
        let ((key, value), new_input) = parse_dict_field(input)?;
        input = new_input.trim_start();

        map.insert(key, value);
    }

    // Unwrap safety: we just checked we can do this
    Some((map, input.strip_prefix('}').unwrap().trim_start()))
}

fn parse_dict_field(input: &str) -> Option<((String, Value), &str)> {
    let (key, input) = parse_string(input)?;
    let input = input.trim_start();
    let (value, input) = parse_value(input)?;
    let input = input.trim_start();

    Some(((key, value), input))
}

fn parse_value(input: &str) -> Option<(Value, &str)> {
    parse_string(input)
        .map(|(v, r)| (Value::String(v), r))
        .or_else(|| parse_dict(input).map(|(v, r)| (Value::Dict(v), r)))
}
